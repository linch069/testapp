package com.example.testapp;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.testapp.data.DataTest;

@RestController
@RequestMapping("testapp")
public class TestAppController {
	
	
	@GetMapping("working")
	public ResponseEntity<Object> testApp() {
		
		DataTest test = new DataTest();
		
		test.setAge("20");
		test.setDepartment("IT");
		test.setId("1001");
		test.setName("Ankita");
		
		return new ResponseEntity<Object>(test, HttpStatus.OK);
	}

}
